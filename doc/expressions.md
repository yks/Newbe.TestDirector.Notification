# 条件表达式

## 新增需求

ChangedFields["BUG"]["BG_BUG_ID"] != null && !string.IsNullOrEmpty(ChangedFields["BUG"]["BG_BUG_ID"].NowValue) && string.IsNullOrEmpty(ChangedFields["BUG"]["BG_BUG_ID"].OldValue)

## 需求已受理

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "受理"

## 类型变更为需求

ChangedFields["BUG"]["BG_USER_11"] != null && ChangedFields["BUG"]["BG_USER_11"].NowValue == "需求"

## 类型变更为缺陷

ChangedFields["BUG"]["BG_USER_11"] != null && ChangedFields["BUG"]["BG_USER_11"].NowValue == "缺陷"

## 状态变为测试中

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "测试中"

## 状态变为待前端明确

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "待前端明确"

## 状态变为完成

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "完成"

## 状态变为拒绝

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "拒绝"

## 状态变为延期

ChangedFields["BUG"]["BG_USER_02"] != null && ChangedFields["BUG"]["BG_USER_02"].NowValue == "延期"

## 前端确认解决情况变更

ChangedFields["BUG"]["BG_USER_01"] != null && ChangedFields["BUG"]["BG_USER_01"].NowValue != ChangedFields["BUG"]["BG_USER_01"].OldValue
