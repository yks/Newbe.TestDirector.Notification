﻿using FluentAssertions;
using Newbe.TestDirector.Notification.Web.Domain;
using System.Linq;
using Newbe.TestDirector.Notification.Web.Domain.Impl;
using Xunit;

namespace Newbe.TestDirector.Notification.Web.Intergration
{
    public class AccessDbTests
    {
        [Fact]
        public void ListData()
        {
            var db = new TestDirectorAcceessDb
            {
                FilePath = @"D:\Repos\Newbe.TestDirector.Notification\src\TestDir.mdb",
            };
            IBugDao bugDao = new BugDao(db, new BugTableColumnsFactory(db));
            var bugDatas = bugDao.List().ToArray();
            bugDatas.Count().Should().NotBe(0);
            var first = bugDatas.First();
            var last = bugDatas.Last();
            first.Fields.Select(x => x.TableColumn)
                .All(x => last.Fields.Select(a => a.TableColumn).Contains(x))
                .Should().BeTrue();
        }
    }
}
