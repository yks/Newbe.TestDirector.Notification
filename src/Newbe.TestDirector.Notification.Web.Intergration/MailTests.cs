﻿using Newbe.TestDirector.Notification.Web.Domain;
using Xunit;

namespace Newbe.TestDirector.Notification.Web.Intergration
{
    public class MailTests
    {
        [Fact]
        public void Test()
        {
            var qqMailer = new QqMailer(new MailerOptions
            {
                Host = "smtp.exmail.qq.com",
                Port = 25,
                SenderUsername = "td@teleware.cn",
                FromAddress = "td@teleware.cn",
                SenderPassord = "xxxxxxxxxxxx",
            });
            var html = "<table>" +
                       "<tr>" +
                       "<td>中文内容2" +
                       "</td>" +
                       "<td>中文内容1" +
                       "</td>" +
                       "</tr>" +
                       "</table>";
            qqMailer.Send(new MailerSendContext
            {
                Body = html,
                ToAddress = "472158246@qq.com",
                Title = "中文标题"
            });
        }
    }
}
