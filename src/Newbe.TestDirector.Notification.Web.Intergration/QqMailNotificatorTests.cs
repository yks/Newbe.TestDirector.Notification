﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using Newbe.TestDirector.Notification.Web.Jobs;
using System.Collections.Generic;
using Xunit;

namespace Newbe.TestDirector.Notification.Web.Intergration
{
    public class QqMailNotificatorTests
    {
        [Fact]
        public void Test()
        {
            var qqMailNotificator = new QqMailNotificator(new SystemOptions
            {
                MailBodyTemplateFilePath = @"D:\Repos\Newbe.TestDirector.Notification\src\Newbe.TestDirector.Notification.Web\Views\MailBodyTemplate.cshtml",
                MailerOptions = new MailerOptions
                {
                    SenderPassord = Contansts.TdMailSenderMailPassword,
                    FromAddress = Contansts.TdMailSenderMailAddress,
                    SenderUsername = Contansts.TdMailSenderMailAddress,
                }
            }, null);
            var codeCol = new TableColumn
            {
                ColumnName = "CODE",
                DisplayName = "代码",
                TableName = "BUG"
            };
            var nameCol = new TableColumn
            {
                ColumnName = "NAME",
                DisplayName = "名称",
                TableName = "BUG"
            };
            var mailerSenderBookCollection = new MailerSenderBookCollection
            {
                {
                    new MailerInfoEntity
                    {
                        MailAddress = "472158246@qq.com"
                    },
                    new MailSceneEntity
                    {
                        Name = "Test1",
                        ShowColumns = new List<ShowColumnEntity>
                        {
                            new ShowColumnEntity
                            {
                                TableColumn = codeCol
                            },
                            new ShowColumnEntity
                            {
                                TableColumn = nameCol
                            }
                        }
                    },
                    new BugChangeResultCollection(new List<BugChangeResult>
                    {
                        new BugChangeResult(new BugData
                        {
                            Id = "1",
                            Fields = new[]
                            {
                                new BugDataField
                                {
                                    TableColumn = nameCol,
                                    Value = "BUG student",
                                },
                                new BugDataField
                                {
                                    TableColumn = codeCol,
                                    Value = "CODE student",
                                }
                            }
                        })
                        {
                            Id = "1",
                        },
                        new BugChangeResult( new BugData
                        {
                            Id = "3",
                            Fields = new[]
                            {
                                new BugDataField
                                {
                                    TableColumn = nameCol,
                                    Value = "BUG 都懂得",
                                },
                                new BugDataField
                                {
                                    TableColumn = codeCol,
                                    Value = "CODE 圈完钱文档",
                                }
                            }
                        })
                        {
                            Id = "3",
                        }
                    })
                },
                {
                    new MailerInfoEntity
                    {
                        MailAddress = "472158246@qq.com"
                    },
                    new MailSceneEntity
                    {
                        Name = "Test2",
                        ShowColumns = new List<ShowColumnEntity>
                        {
                            new ShowColumnEntity
                            {
                                TableColumn = codeCol
                            },
                        }
                    },
                    new BugChangeResultCollection(new List<BugChangeResult>
                    {
                        new BugChangeResult(   new BugData
                        {
                            Id = "2",
                            Fields = new[]
                            {
                                new BugDataField
                                {
                                    TableColumn = nameCol,
                                    Value = "BUG stude123nt",
                                },
                                new BugDataField
                                {
                                    TableColumn = codeCol,
                                    Value = "CODE stud123ent",
                                }
                            }
                        })
                        {
                            Id = "2",
                        },
                    })
                }
            };

            qqMailNotificator.Notificat(new NotificatContext
            {
                MailerSenderBookCollection = mailerSenderBookCollection
            });
        }
    }
}
