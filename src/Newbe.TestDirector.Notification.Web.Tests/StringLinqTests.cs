﻿using FluentAssertions;
using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Xunit;

namespace Newbe.TestDirector.Notification.Web.Tests
{
    public class StringLinqTests
    {
        [Fact]
        public void Test()
        {
            var bugChangeResultCollection = new BugChangeResultCollection(new List<BugChangeResult>
            {
                new BugChangeResult(null)
                {
                    Id = "1",
                    ChangedFields = new BugChangeFieldTableCollection(new List<BugChangeFieldResult>
                    {
                        new BugChangeFieldResult
                        {
                            TableColumn = new TableColumn
                            {
                                ColumnName = "NAME",
                                TableName = "BUG",
                                DisplayName = "NAME"
                            },
                            NowValue = string.Empty,
                            OldValue = "HEHEDA"
                        }
                    })
                }
            });
            var list = bugChangeResultCollection.Where("ChangedFields[\"BUG\"][\"NAME\"].OldValue == \"HEHEDA\"")
                .ToList();
            list.Count().Should().Be(1);
        }
    }
}
