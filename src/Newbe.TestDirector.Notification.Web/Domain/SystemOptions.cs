﻿namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class SystemOptions
    {
        public string TestDirectorDatabaseFilePath { get; set; }
        public string MailJobCronExpression { get; set; }
        public string MailBodyTemplateFilePath { get; set; }
        public MailerOptions MailerOptions { get; set; } = new MailerOptions();
    }
}
