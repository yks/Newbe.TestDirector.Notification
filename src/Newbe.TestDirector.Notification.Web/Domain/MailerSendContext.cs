﻿namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class MailerSendContext
    {
        public string ToAddress { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
