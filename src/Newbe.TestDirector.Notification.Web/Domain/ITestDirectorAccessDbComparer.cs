﻿using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface ITestDirectorAccessDbComparer
    {
        BugChangeResultCollection Compare(IEnumerable<BugData> old, IEnumerable<BugData> now);
    }
}
