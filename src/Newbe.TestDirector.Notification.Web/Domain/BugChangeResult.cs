﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class BugChangeFieldTableCollection : IEnumerable<BugChangeFieldResult>
    {
        private readonly IEnumerable<BugChangeFieldResult> _list;

        public BugChangeFieldTableCollection(IEnumerable<BugChangeFieldResult> list)
        {
            _list = list;
        }

        public IEnumerator<BugChangeFieldResult> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public BugChangeFieldResultCollection this[string table]
        {
            get
            {
                return new BugChangeFieldResultCollection(_list.Where(x =>
                    x.TableColumn.TableName == table));
            }
        }

        public class BugChangeFieldResultCollection : IEnumerable<BugChangeFieldResult>
        {
            private readonly IEnumerable<BugChangeFieldResult> _list;

            public BugChangeFieldResultCollection(IEnumerable<BugChangeFieldResult> list)
            {
                _list = list;
            }

            public IEnumerator<BugChangeFieldResult> GetEnumerator()
            {
                return _list.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public BugChangeFieldResult this[string col]
            {
                get { return _list.FirstOrDefault(x => x.TableColumn.ColumnName == col); }
            }
        }
    }

    public class BugChangeResult
    {
        public BugChangeResult(BugData nowData)
        {
            NowData = nowData;
        }

        public string Id { get; set; }

        public BugData NowData { get; }

        public BugChangeFieldTableCollection ChangedFields { get; set; } =
            new BugChangeFieldTableCollection(Enumerable.Empty<BugChangeFieldResult>());

        public bool HasChanged => ChangedFields.Any();
    }
}
