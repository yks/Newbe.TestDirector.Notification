﻿namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class MailerOptions
    {
        public string FromAddress { get; set; }
        public string Host { get; set; } = "smtp.exmail.qq.com";
        public ushort Port { get; set; } = 25;
        public string SenderUsername { get; set; }
        public string SenderPassord { get; set; }
    }
}
