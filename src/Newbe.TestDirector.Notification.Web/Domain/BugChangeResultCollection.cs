﻿using System.Collections;
using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class BugChangeResultCollection : IEnumerable<BugChangeResult>
    {
        public BugChangeResultCollection(IEnumerable<BugChangeResult> list)
        {
            _list = list;
        }

        private readonly IEnumerable<BugChangeResult> _list;

        public IEnumerator<BugChangeResult> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
