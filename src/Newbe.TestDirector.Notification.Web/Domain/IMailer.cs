﻿namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface IMailer
    {
        MailerOptions MailerOptions { get; }
        void Send(MailerSendContext context);
    }
}
