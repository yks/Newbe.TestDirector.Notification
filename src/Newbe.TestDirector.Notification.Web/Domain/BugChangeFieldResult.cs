﻿using Newbe.TestDirector.Notification.Web.Entities;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class BugChangeFieldResult
    {
        public TableColumn TableColumn { get; set; }
        public string OldValue { get; set; }
        public string NowValue { get; set; }
    }
}
