﻿using Newbe.TestDirector.Notification.Web.Entities;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class BugDataField
    {
        public TableColumn TableColumn { get; set; }
        public string Value { get; set; }
    }
}
