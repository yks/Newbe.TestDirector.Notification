﻿using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface IBugDao
    {
        IEnumerable<BugData> List();

    }
}
