﻿using System.Data;
using System.Data.OleDb;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public static class TestDirectorAccessDbExtensions
    {
        public static IDbConnection GetConnection(this TestDirectorAcceessDb db)
        {
            return new OleDbConnection($"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={db.FilePath}");
        }
    }
}
