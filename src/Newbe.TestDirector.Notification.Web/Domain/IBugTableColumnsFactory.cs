﻿using Newbe.TestDirector.Notification.Web.Entities;
using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface IBugTableColumnsFactory
    {
        IEnumerable<TableColumn> GeTableColumns();
    }
}
