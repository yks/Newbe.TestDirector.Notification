﻿using System.Net;
using System.Net.Mail;
using System.Text;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    internal class QqMailer : IMailer
    {
        public QqMailer(MailerOptions mailerOptions)
        {
            MailerOptions = mailerOptions;
        }

        public MailerOptions MailerOptions { get; }

        public void Send(MailerSendContext context)
        {
            var msg = new MailMessage();
            msg.To.Add(context.ToAddress);
            msg.From = new MailAddress(MailerOptions.FromAddress);
            msg.Subject = context.Title;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = context.Body;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;

            using (var client = new SmtpClient())
            {
                client.Host = MailerOptions.Host;
                client.Port = MailerOptions.Port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(MailerOptions.SenderUsername, MailerOptions.SenderPassord);
                client.Send(msg);
            }
        }
    }
}
