﻿using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface IHistoryTestDirectorDatabaseStore
    {
        IEnumerable<TestDirectorAcceessDb> GeTestDirectorAcceessDbs();
        void AddNew(TestDirectorAcceessDb testDirectorAcceessDb);
        TestDirectorAcceessDb GetLastest();
        void RemoveLastest();
    }
}
