﻿using Dapper;
using Newbe.TestDirector.Notification.Web.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Newbe.TestDirector.Notification.Web.Domain.Impl
{
    internal class BugTableColumnsFactory : IBugTableColumnsFactory
    {
        private readonly TestDirectorAcceessDb _testDirectorAcceessDb;

        public BugTableColumnsFactory(TestDirectorAcceessDb testDirectorAcceessDb)
        {
            _testDirectorAcceessDb = testDirectorAcceessDb;
        }

        IEnumerable<TableColumn> IBugTableColumnsFactory.GeTableColumns()
        {
            using (var conn = _testDirectorAcceessDb.GetConnection())
            {
                var systemFields = conn.Query<SystemField>("select * from system_field");
                return systemFields.Where(x => x.SF_TABLE_NAME == "BUG").Select(x => new TableColumn
                {
                    ColumnName = x.SF_COLUMN_NAME,
                    TableName = x.SF_TABLE_NAME,
                    DisplayName = x.SF_USER_LABEL,
                }).ToList();
            }
        }

        internal class SystemField
        {
            /// <summary>
            /// TableName
            /// </summary>
            public string SF_TABLE_NAME { get; set; }

            /// <summary>
            /// ColumnName
            /// </summary>
            public string SF_COLUMN_NAME { get; set; }

            /// <summary>
            /// DisplayName
            /// </summary>
            public string SF_USER_LABEL { get; set; }
        }
    }
}
