﻿namespace Newbe.TestDirector.Notification.Web.Domain.BugChangedScenes
{
    public interface IBugChangedScene
    {
        /// <summary>
        /// Find Scene-Matched BugChangeResultCollection
        /// </summary>
        /// <param name="bugChangeResultCollection"></param>
        /// <returns></returns>
        BugChangeResultCollection Match(BugChangeResultCollection bugChangeResultCollection);
    }
}
