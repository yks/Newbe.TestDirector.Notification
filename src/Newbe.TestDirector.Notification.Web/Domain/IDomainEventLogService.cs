﻿namespace Newbe.TestDirector.Notification.Web.Domain
{
    public interface IDomainEventLogService
    {
        void AddDoaminEventLog(string title, string message);
    }
}
