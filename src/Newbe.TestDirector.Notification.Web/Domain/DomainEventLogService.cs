﻿using Newbe.TestDirector.Notification.Web.Entities;
using System;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    internal class DomainEventLogService : IDomainEventLogService
    {
        private readonly IEfContext _efContext;
        private readonly IPrimaryKeyGenerator _primaryKeyGenerator;

        public DomainEventLogService(IEfContext efContext, IPrimaryKeyGenerator primaryKeyGenerator)
        {
            _efContext = efContext;
            _primaryKeyGenerator = primaryKeyGenerator;
        }

        public void AddDoaminEventLog(string title, string message)
        {
            _efContext.DomainEventLogs.Add(new DomainEventLog
            {
                Id = _primaryKeyGenerator.GetKey(),
                Message = message,
                Title = title,
                Timestamp = DateTimeOffset.Now.DateTime,
            });
            _efContext.SaveChanges();
        }
    }
}
