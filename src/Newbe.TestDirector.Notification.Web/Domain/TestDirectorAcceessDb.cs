﻿using System;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class TestDirectorAcceessDb
    {
        public DateTimeOffset TimeStamp { get; set; }
        public string FilePath { get; set; }
    }
}
