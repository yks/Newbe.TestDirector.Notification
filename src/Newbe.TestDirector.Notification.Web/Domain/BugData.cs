﻿using System.Collections.Generic;
using System.Linq;

namespace Newbe.TestDirector.Notification.Web.Domain
{
    public class BugData
    {
        public string Id { get; set; }
        public IEnumerable<BugDataField> Fields { get; set; } = Enumerable.Empty<BugDataField>();

        public string this[string index]
        {
            get { return Fields.First(x => x.TableColumn.ColumnName == index).Value; }
        }
    }
}
