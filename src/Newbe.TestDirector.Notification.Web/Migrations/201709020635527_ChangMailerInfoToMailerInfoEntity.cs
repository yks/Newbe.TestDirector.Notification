namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangMailerInfoToMailerInfoEntity : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MailerInfoes", newName: "MailerInfoEntities");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.MailerInfoEntities", newName: "MailerInfoes");
        }
    }
}
