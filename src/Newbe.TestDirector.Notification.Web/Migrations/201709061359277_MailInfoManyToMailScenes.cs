﻿namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MailInfoManyToMailScenes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropIndex("dbo.MailerInfoEntities", new[] { "MailSceneEntity_Id" });
            CreateTable(
                "dbo.MailSceneEntityMailerInfoEntities",
                c => new
                {
                    MailSceneEntity_Id = c.String(nullable: false, maxLength: 40),
                    MailerInfoEntity_Id = c.String(nullable: false, maxLength: 40),
                })
                .PrimaryKey(t => new { t.MailSceneEntity_Id, t.MailerInfoEntity_Id })
                .ForeignKey("dbo.MailSceneEntities", t => t.MailSceneEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.MailerInfoEntities", t => t.MailerInfoEntity_Id, cascadeDelete: true)
                .Index(t => t.MailSceneEntity_Id)
                .Index(t => t.MailerInfoEntity_Id);

            DropColumn("dbo.MailerInfoEntities", "MailSceneEntity_Id");
        }

        public override void Down()
        {
            AddColumn("dbo.MailerInfoEntities", "MailSceneEntity_Id", c => c.String(maxLength: 40));
            DropForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailerInfoEntity_Id", "dbo.MailerInfoEntities");
            DropForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropIndex("dbo.MailSceneEntityMailerInfoEntities", new[] { "MailerInfoEntity_Id" });
            DropIndex("dbo.MailSceneEntityMailerInfoEntities", new[] { "MailSceneEntity_Id" });
            DropTable("dbo.MailSceneEntityMailerInfoEntities");
            CreateIndex("dbo.MailerInfoEntities", "MailSceneEntity_Id");
            AddForeignKey("dbo.MailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities", "Id");
        }
    }
}
