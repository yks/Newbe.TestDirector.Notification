﻿namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMailSceneMailerInfoSources : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailerInfoEntity_Id", "dbo.MailerInfoEntities");
            DropIndex("dbo.MailSceneEntityMailerInfoEntities", new[] { "MailSceneEntity_Id" });
            DropIndex("dbo.MailSceneEntityMailerInfoEntities", new[] { "MailerInfoEntity_Id" });
            CreateTable(
                "dbo.MailSceneMailerInfoSourceEntities",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 40),
                    MailSceneMailerInfoSourceType = c.Int(nullable: false),
                    MailerInfoFromColumn_TableName = c.String(maxLength: 200),
                    MailerInfoFromColumn_ColumnName = c.String(maxLength: 200),
                    MailerInfoFromColumn_DisplayName = c.String(maxLength: 200),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MailSceneEntities", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            Sql("insert into  MailSceneMailerInfoSourceEntities select Id , 0  as MailSceneMailerInfoSourceType  ,null,null,null  from  MailSceneEntities ");
            CreateTable(
                "dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities",
                c => new
                {
                    MailSceneMailerInfoSourceEntity_Id = c.String(nullable: false, maxLength: 40),
                    MailerInfoEntity_Id = c.String(nullable: false, maxLength: 40),
                })
                .PrimaryKey(t => new { t.MailSceneMailerInfoSourceEntity_Id, t.MailerInfoEntity_Id })
                .ForeignKey("dbo.MailSceneMailerInfoSourceEntities", t => t.MailSceneMailerInfoSourceEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.MailerInfoEntities", t => t.MailerInfoEntity_Id, cascadeDelete: true)
                .Index(t => t.MailSceneMailerInfoSourceEntity_Id)
                .Index(t => t.MailerInfoEntity_Id);

            AddColumn("dbo.MailSceneEntities", "SourceDataWhereExpression", c => c.String());
            DropTable("dbo.MailSceneEntityMailerInfoEntities");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.MailSceneEntityMailerInfoEntities",
                c => new
                {
                    MailSceneEntity_Id = c.String(nullable: false, maxLength: 40),
                    MailerInfoEntity_Id = c.String(nullable: false, maxLength: 40),
                })
                .PrimaryKey(t => new { t.MailSceneEntity_Id, t.MailerInfoEntity_Id });

            DropForeignKey("dbo.MailSceneMailerInfoSourceEntities", "Id", "dbo.MailSceneEntities");
            DropForeignKey("dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities", "MailerInfoEntity_Id", "dbo.MailerInfoEntities");
            DropForeignKey("dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities", "MailSceneMailerInfoSourceEntity_Id", "dbo.MailSceneMailerInfoSourceEntities");
            DropIndex("dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities", new[] { "MailerInfoEntity_Id" });
            DropIndex("dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities", new[] { "MailSceneMailerInfoSourceEntity_Id" });
            DropIndex("dbo.MailSceneMailerInfoSourceEntities", new[] { "Id" });
            DropColumn("dbo.MailSceneEntities", "SourceDataWhereExpression");
            DropTable("dbo.MailSceneMailerInfoSourceEntityMailerInfoEntities");
            DropTable("dbo.MailSceneMailerInfoSourceEntities");
            CreateIndex("dbo.MailSceneEntityMailerInfoEntities", "MailerInfoEntity_Id");
            CreateIndex("dbo.MailSceneEntityMailerInfoEntities", "MailSceneEntity_Id");
            AddForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailerInfoEntity_Id", "dbo.MailerInfoEntities", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MailSceneEntityMailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities", "Id", cascadeDelete: true);
        }
    }
}
