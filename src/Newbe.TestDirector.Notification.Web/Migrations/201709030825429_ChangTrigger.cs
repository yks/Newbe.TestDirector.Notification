namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangTrigger : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChangeTriggerConditionEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropIndex("dbo.ChangeTriggerConditionEntities", new[] { "MailSceneEntity_Id" });
            AddColumn("dbo.MailSceneEntities", "PredicateExpression", c => c.String(nullable: false));
            DropTable("dbo.ChangeTriggerConditionEntities");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ChangeTriggerConditionEntities",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TableColumn_TableName = c.String(nullable: false, maxLength: 200),
                        TableColumn_ColumnName = c.String(nullable: false, maxLength: 200),
                        TableColumn_DisplayName = c.String(maxLength: 200),
                        OldValue = c.String(),
                        NowValue = c.String(),
                        MailSceneEntity_Id = c.String(nullable: false, maxLength: 40),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.MailSceneEntities", "PredicateExpression");
            CreateIndex("dbo.ChangeTriggerConditionEntities", "MailSceneEntity_Id");
            AddForeignKey("dbo.ChangeTriggerConditionEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities", "Id", cascadeDelete: true);
        }
    }
}
