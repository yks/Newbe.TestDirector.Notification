namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDomainEventLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DomainEventLogs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 40),
                        Timestamp = c.DateTime(nullable: false),
                        Title = c.String(nullable: false),
                        Message = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DomainEventLogs");
        }
    }
}
