﻿using Newbe.TestDirector.Notification.Web.Entities;

namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Newbe.TestDirector.Notification.Web.Entities.DContext";
        }

        protected override void Seed(DContext context)
        {

        }
    }
}
