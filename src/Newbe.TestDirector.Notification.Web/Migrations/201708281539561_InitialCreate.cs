namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MailerInfoes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 40),
                        Name = c.String(nullable: false, maxLength: 200),
                        MailAddress = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MailerInfoes");
        }
    }
}
