﻿namespace Newbe.TestDirector.Notification.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMailScene : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChangeTriggerConditionEntities",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    TableColumn_TableName = c.String(nullable: false, maxLength: 200),
                    TableColumn_ColumnName = c.String(nullable: false, maxLength: 200),
                    TableColumn_DisplayName = c.String(maxLength: 200),
                    OldValue = c.String(),
                    NowValue = c.String(),
                    MailSceneEntity_Id = c.String(nullable: false, maxLength: 40),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MailSceneEntities", t => t.MailSceneEntity_Id, cascadeDelete: true)
                .Index(t => t.MailSceneEntity_Id);

            CreateTable(
                "dbo.MailSceneEntities",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 40),
                    Code = c.String(nullable: false, maxLength: 200),
                    Name = c.String(nullable: false, maxLength: 200),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ShowColumnEntities",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    TableColumn_TableName = c.String(nullable: false, maxLength: 200),
                    TableColumn_ColumnName = c.String(nullable: false, maxLength: 200),
                    TableColumn_DisplayName = c.String(maxLength: 200),
                    MailSceneEntity_Id = c.String(nullable: false, maxLength: 40),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MailSceneEntities", t => t.MailSceneEntity_Id, cascadeDelete: true)
                .Index(t => t.MailSceneEntity_Id);

            AddColumn("dbo.MailerInfoEntities", "MailSceneEntity_Id", c => c.String(maxLength: 40));
            CreateIndex("dbo.MailerInfoEntities", "MailSceneEntity_Id");
            AddForeignKey("dbo.MailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.ShowColumnEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropForeignKey("dbo.MailerInfoEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropForeignKey("dbo.ChangeTriggerConditionEntities", "MailSceneEntity_Id", "dbo.MailSceneEntities");
            DropIndex("dbo.ShowColumnEntities", new[] { "MailSceneEntity_Id" });
            DropIndex("dbo.MailerInfoEntities", new[] { "MailSceneEntity_Id" });
            DropIndex("dbo.ChangeTriggerConditionEntities", new[] { "MailSceneEntity_Id" });
            DropColumn("dbo.MailerInfoEntities", "MailSceneEntity_Id");
            DropTable("dbo.ShowColumnEntities");
            DropTable("dbo.MailSceneEntities");
            DropTable("dbo.ChangeTriggerConditionEntities");
        }
    }
}
