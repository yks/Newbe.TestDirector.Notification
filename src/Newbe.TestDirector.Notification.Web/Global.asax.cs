﻿using Autofac.Integration.Web;
using System;

namespace Newbe.TestDirector.Notification.Web
{
    public class Global : System.Web.HttpApplication, IContainerProviderAccessor
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        public IContainerProvider ContainerProvider
        {
            get { return StaticContainerProvider; }
        }

        internal static IContainerProvider StaticContainerProvider { get; set; }
    }
}
