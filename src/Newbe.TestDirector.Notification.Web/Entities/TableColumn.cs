﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class TableColumn
    {
        protected bool Equals(TableColumn other)
        {
            return string.Equals(TableName, other.TableName) && string.Equals(ColumnName, other.ColumnName) &&
                   string.Equals(DisplayName, other.DisplayName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableColumn)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (TableName != null ? TableName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ColumnName != null ? ColumnName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DisplayName != null ? DisplayName.GetHashCode() : 0);
                return hashCode;
            }
        }

        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
    }
}
