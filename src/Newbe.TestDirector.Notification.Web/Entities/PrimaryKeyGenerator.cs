﻿using System;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    internal class PrimaryKeyGenerator : IPrimaryKeyGenerator
    {
        string IPrimaryKeyGenerator.GetKey()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
