﻿using System;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class DomainEventLog : IEntity
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
