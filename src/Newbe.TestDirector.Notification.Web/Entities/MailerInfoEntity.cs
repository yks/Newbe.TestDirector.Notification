﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class MailerInfoEntity : IEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string MailAddress { get; set; }
    }
}
