﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class ShowColumnEntity : IEntity
    {
        public string Id { get; set; }
        public TableColumn TableColumn { get; set; }
    }
}
