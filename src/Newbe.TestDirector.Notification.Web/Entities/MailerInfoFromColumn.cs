﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class MailerInfoFromColumn
    {
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
    }
}
