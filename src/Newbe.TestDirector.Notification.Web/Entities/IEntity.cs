﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
