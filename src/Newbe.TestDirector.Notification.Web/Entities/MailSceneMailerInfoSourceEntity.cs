﻿using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class MailSceneMailerInfoSourceEntity : IEntity
    {
        public string Id { get; set; }

        public MailSceneMailerInfoSourceType MailSceneMailerInfoSourceType { get; set; }

        public MailerInfoFromColumn MailerInfoFromColumn { get; set; }

        public virtual ICollection<MailerInfoEntity> MailerInfos { get; set; }
            = new List<MailerInfoEntity>();
    }
}
