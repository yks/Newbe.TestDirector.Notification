﻿using Newbe.TestDirector.Notification.Web.Migrations;
using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class DContext : DbContext, IEfContext
    {
        public DContext() : base("Default")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DContext, Configuration>());
            Database.Log = x => Debug.WriteLine(x);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var mailerInfos = modelBuilder.Entity<MailerInfoEntity>();
            mailerInfos.Property(x => x.Id)
                .HasMaxLength(DbStringLength.Id);
            mailerInfos.Property(x => x.MailAddress)
                .HasMaxLength(DbStringLength.Normal)
                .IsRequired();
            mailerInfos.Property(x => x.Name)
                .HasMaxLength(DbStringLength.Name)
                .IsRequired();

            var tableColumn = modelBuilder.ComplexType<TableColumn>();
            tableColumn.Property(x => x.ColumnName)
                .HasMaxLength(DbStringLength.Normal)
                .IsRequired();
            tableColumn.Property(x => x.DisplayName)
                .HasMaxLength(DbStringLength.Normal);
            tableColumn.Property(x => x.TableName)
                .HasMaxLength(DbStringLength.Normal)
                .IsRequired();

            var showColumns = modelBuilder.Entity<ShowColumnEntity>();

            var mailScenes = modelBuilder.Entity<MailSceneEntity>();
            mailScenes
                .Property(x => x.Id)
                .HasMaxLength(DbStringLength.Id)
                .IsRequired();
            mailScenes.Property(x => x.Name)
                .HasMaxLength(DbStringLength.Name)
                .IsRequired();
            mailScenes.Property(x => x.Code)
                .HasMaxLength(DbStringLength.Normal)
                .IsRequired();
            mailScenes.Property(x => x.PredicateExpression)
                .IsMaxLength()
                .IsRequired();
            mailScenes.Property(x => x.SourceDataWhereExpression)
                .IsMaxLength();
            mailScenes
                .HasMany(x => x.ShowColumns)
                .WithRequired();
            mailScenes
                .HasRequired(x => x.MailSceneMailerInfoSource)
                .WithRequiredPrincipal()
                .WillCascadeOnDelete();

            var mailerInfoFromColumn = modelBuilder.ComplexType<MailerInfoFromColumn>();
            mailerInfoFromColumn.Property(x => x.ColumnName)
                .HasMaxLength(DbStringLength.Normal);
            mailerInfoFromColumn.Property(x => x.DisplayName)
                .HasMaxLength(DbStringLength.Normal);
            mailerInfoFromColumn.Property(x => x.TableName)
                .HasMaxLength(DbStringLength.Normal);

            var mailSceneMailerInfoSources = modelBuilder.Entity<MailSceneMailerInfoSourceEntity>();
            mailSceneMailerInfoSources.Property(x => x.MailSceneMailerInfoSourceType)
                .IsRequired();
            mailSceneMailerInfoSources
                .HasMany(x => x.MailerInfos)
                .WithMany();

            var domainEventLogs = modelBuilder.Entity<DomainEventLog>();
            domainEventLogs.Property(x => x.Title)
                .IsMaxLength()
                .IsRequired();
            domainEventLogs.Property(x => x.Message)
                .IsMaxLength()
                .IsRequired();
            domainEventLogs.Property(x => x.Id)
                .HasMaxLength(DbStringLength.Id)
                .IsRequired();
        }

        private static class DbStringLength
        {
            public const int Small = 40;
            public const int Normal = 200;
            public const int Big = 4000;
            public const int Id = Small;
            public const int Name = Normal;
            public const int Remark = Big;
        }

        public void Init()
        {
            Console.WriteLine(MailerInfos.Any());
        }

        public IDbSet<MailerInfoEntity> MailerInfos { get; set; }
        public IDbSet<MailSceneEntity> MailScenes { get; set; }
        public IDbSet<ShowColumnEntity> ShowColumns { get; set; }
        public IDbSet<MailSceneMailerInfoSourceEntity> MailSceneMailerInfoSources { get; set; }
        public IDbSet<DomainEventLog> DomainEventLogs { get; set; }
    }
}
