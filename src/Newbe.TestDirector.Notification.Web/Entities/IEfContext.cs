﻿using System;
using System.Data.Entity;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    public interface IEfContext : IDisposable
    {
        void Init();
        IDbSet<MailerInfoEntity> MailerInfos { get; }
        IDbSet<MailSceneEntity> MailScenes { get; set; }
        IDbSet<ShowColumnEntity> ShowColumns { get; set; }
        IDbSet<MailSceneMailerInfoSourceEntity> MailSceneMailerInfoSources { get; set; }
        IDbSet<DomainEventLog> DomainEventLogs { get; set; }
        int SaveChanges();
    }
}
