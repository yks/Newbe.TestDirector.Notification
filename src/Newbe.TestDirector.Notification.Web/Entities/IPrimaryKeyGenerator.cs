﻿namespace Newbe.TestDirector.Notification.Web.Entities
{
    public interface IPrimaryKeyGenerator
    {
        string GetKey();
    }
}
