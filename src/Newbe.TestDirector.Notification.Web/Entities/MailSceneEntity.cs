﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Domain.BugChangedScenes;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace Newbe.TestDirector.Notification.Web.Entities
{
    public class MailSceneEntity : IEntity, IBugChangedScene
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string PredicateExpression { get; set; }
        public string SourceDataWhereExpression { get; set; }

        public virtual MailSceneMailerInfoSourceEntity MailSceneMailerInfoSource { get; set; }

        public ICollection<ShowColumnEntity> ShowColumns { get; set; }
            = new List<ShowColumnEntity>();

        public BugChangeResultCollection Match(BugChangeResultCollection bugChangeResultCollection)
        {
            var bugChangeResults = bugChangeResultCollection
                .Where(x => x.HasChanged)
                .Where(PredicateExpression);
            if (!string.IsNullOrEmpty(SourceDataWhereExpression))
            {
                bugChangeResults = bugChangeResults.Where(SourceDataWhereExpression);
            }
            return new BugChangeResultCollection(bugChangeResults);
        }
    }
}
