﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="Receivers.aspx.cs" Inherits="Newbe.TestDirector.Notification.Web.Admin.MailScenes.Receivers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" RepeatDirection="Horizontal">
        <asp:ListItem Value="Static" Selected="True">固定收件人</asp:ListItem>
        <asp:ListItem Value="TableColumn">来自字段</asp:ListItem>
    </asp:RadioButtonList>
    <asp:Panel ID="PanelStatic" runat="server">

        <asp:GridView ID="gvMailerInfos" runat="server" CellPadding="4" DataKeyNames="Id" DataSourceID="MailerInfos" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="选为收件人">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                        <asp:HiddenField ID="HdfId" runat="server" Value='<%# Eval("Id") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <ef:EntityDataSource runat="server"
            ID="MailerInfos"
            ContextTypeName="Newbe.TestDirector.Notification.Web.Entities.DContext"
            EntitySetName="MailerInfos"
            EnableUpdate="True"
            EnableDelete="True"
            EnableInsert="True">
        </ef:EntityDataSource>
    </asp:Panel>
    <asp:Panel ID="PanelTableColumn" runat="server" Visible="False">
        <asp:DropDownList ID="ddlMailerInfoFromTable" runat="server"
            DataTextField="DisplayName"
            DataValueField="ColumnName"
            Width="200">
        </asp:DropDownList>
    </asp:Panel>
    <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" />
</asp:Content>
