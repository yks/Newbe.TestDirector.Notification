﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ShowColumnsConfig.aspx.cs" Inherits="Newbe.TestDirector.Notification.Web.Admin.MailScenes.ShowColumnsConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandArgument='<%# Eval("ColumnName") %>' CommandName="RemoveShowColumn" Text="删除"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ColumnName" HeaderText="列名" />
            <asp:BoundField DataField="DisplayName" HeaderText="显示名称" />
        </Columns>
    </asp:GridView>
    <asp:DropDownList ID="DropDownList1" runat="server"
        DataTextField="DisplayName"
        DataValueField="ColumnName"
        Width="200">
    </asp:DropDownList>
    <asp:Button ID="Button1" runat="server" Text="新字段" OnClick="Button1_Click" />
</asp:Content>
