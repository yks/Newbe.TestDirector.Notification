﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace Newbe.TestDirector.Notification.Web.Admin.MailScenes
{
    public partial class Receivers : System.Web.UI.Page
    {
        public IEfContext EfContext { get; set; }
        public IBugTableColumnsFactory BugTableColumnsFactory { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var mailSceneEntity = GetMailScene();
                TogglePanel(mailSceneEntity.MailSceneMailerInfoSource.MailSceneMailerInfoSourceType);
                ReloadGrid(mailSceneEntity);
                ddlMailerInfoFromTable.DataSource = BugTableColumnsFactory.GeTableColumns().ToArray();
                ddlMailerInfoFromTable.DataBind();
                ddlMailerInfoFromTable.SelectedValue = mailSceneEntity.MailSceneMailerInfoSource.MailerInfoFromColumn.ColumnName;
            }
        }

        private void ReloadGrid(MailSceneEntity mailSceneEntity)
        {
            gvMailerInfos.DataBind();
            var mailerinfoIds = mailSceneEntity.MailSceneMailerInfoSource.MailerInfos
                .Select(x => x.Id)
                .ToArray();
            foreach (GridViewRow row in gvMailerInfos.Rows)
            {
                var mailerId = ((HiddenField)(row.FindControl("HdfId"))).Value;
                if (mailerinfoIds.Contains(mailerId))
                {
                    var ccb = (CheckBox)(row.FindControl("CheckBox1"));
                    ccb.Checked = true;
                }
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var mailSceneMailerInfoSourceType =
                (MailSceneMailerInfoSourceType)Enum.Parse(typeof(MailSceneMailerInfoSourceType),
                    RadioButtonList1.SelectedValue);
            TogglePanel(mailSceneMailerInfoSourceType);
        }

        private void TogglePanel(MailSceneMailerInfoSourceType mailSceneMailerInfoSourceType)
        {
            RadioButtonList1.SelectedValue = mailSceneMailerInfoSourceType.ToString("G");
            PanelTableColumn.Visible = false;
            PanelStatic.Visible = false;
            switch (mailSceneMailerInfoSourceType)

            {
                case MailSceneMailerInfoSourceType.Static:
                    PanelStatic.Visible = true;
                    break;
                case MailSceneMailerInfoSourceType.TableColumn:
                    PanelTableColumn.Visible = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var mailSceneEntity = GetMailScene();
            var mailerInfos = EfContext.MailerInfos.ToArray().ToDictionary(x => x.Id);
            var mailSceneMailerInfoSourceEntity = mailSceneEntity.MailSceneMailerInfoSource;
            foreach (var item in mailSceneMailerInfoSourceEntity.MailerInfos.ToArray())
            {
                mailSceneMailerInfoSourceEntity.MailerInfos.Remove(item);
            }
            foreach (GridViewRow row in gvMailerInfos.Rows)
            {
                var mailerId = ((HiddenField)(row.FindControl("HdfId"))).Value;
                var ccb = (CheckBox)(row.FindControl("CheckBox1"));
                if (ccb.Checked)
                {
                    var mailerInfoEntity = mailerInfos[mailerId];
                    mailSceneMailerInfoSourceEntity.MailerInfos.Add(mailerInfoEntity);
                }
            }
            var mailSceneMailerInfoSourceType =
                (MailSceneMailerInfoSourceType)Enum.Parse(typeof(MailSceneMailerInfoSourceType),
                    RadioButtonList1.SelectedValue);
            mailSceneMailerInfoSourceEntity.MailSceneMailerInfoSourceType =
                mailSceneMailerInfoSourceType;
            var selectColumn = BugTableColumnsFactory.GeTableColumns().First(x => x.ColumnName == ddlMailerInfoFromTable.SelectedValue);
            mailSceneMailerInfoSourceEntity.MailerInfoFromColumn = new MailerInfoFromColumn
            {
                ColumnName = selectColumn.ColumnName,
                DisplayName = selectColumn.DisplayName,
                TableName = selectColumn.TableName,
            };
            EfContext.SaveChanges();
            ReloadGrid(mailSceneEntity);
        }

        private MailSceneEntity GetMailScene()
        {
            var id = Request["Id"];
            var mailSceneEntity = EfContext.MailScenes
                .Include(x => x.MailSceneMailerInfoSource.MailerInfos)
                .First(x => x.Id == id);
            return mailSceneEntity;
        }
    }
}
