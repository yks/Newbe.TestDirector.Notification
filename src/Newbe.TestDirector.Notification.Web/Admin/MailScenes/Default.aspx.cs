﻿using Newbe.TestDirector.Notification.Web.Entities;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Newbe.TestDirector.Notification.Web.Admin.MailScenes
{
    public partial class Default : System.Web.UI.Page
    {
        public IEfContext EfContext { get; set; }

        public IPrimaryKeyGenerator PrimaryKeyGenerator { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = e.CommandArgument.ToString();
            switch (e.CommandName)
            {
                case "ConfigShowColumns":
                    Response.Redirect($"ShowColumnsConfig.aspx?id={id}", true);
                    break;
                case "ConfigRevicer":
                    Response.Redirect($"Receivers.aspx?id={id}", true);
                    break;
            }
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["Id"] = PrimaryKeyGenerator.GetKey();
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            var id = e.Values["Id"].ToString();
            EfContext.MailScenes.Single(x => x.Id == id).MailSceneMailerInfoSource = new MailSceneMailerInfoSourceEntity
            {
                MailSceneMailerInfoSourceType = MailSceneMailerInfoSourceType.Static,
                MailerInfoFromColumn = new MailerInfoFromColumn(),
            };
            EfContext.SaveChanges();
            GridView1.DataBind();
        }
    }
}
