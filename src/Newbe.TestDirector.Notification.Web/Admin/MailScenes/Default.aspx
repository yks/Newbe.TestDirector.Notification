﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Newbe.TestDirector.Notification.Web.Admin.MailScenes.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ef:EntityDataSource runat="server"
        ID="DataSource"
        ContextTypeName="Newbe.TestDirector.Notification.Web.Entities.DContext"
        EntitySetName="MailScenes"
        EnableUpdate="True"
        EnableInsert="True"
        EnableDelete="True" OrderBy="it.Name">
    </ef:EntityDataSource>
    <asp:GridView ID="GridView1" runat="server"
        DataSourceID="DataSource"
        OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server"
                        CausesValidation="false"
                        CommandName="ConfigRevicer"
                        CommandArgument='<%# Eval("ID") %>'
                        Text='<%# Eval("Id", "收件人") %>'>

                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButto2" runat="server"
                        CausesValidation="false"
                        CommandName="ConfigShowColumns"
                        CommandArgument='<%# Eval("ID") %>'
                        Text='<%# Eval("Id", "内容配置") %>'>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="Code" HeaderText="代码" />
            <asp:BoundField DataField="Name" HeaderText="名称" />
            <asp:BoundField DataField="PredicateExpression" HeaderText="BUG变化检测表达式" />
            <asp:BoundField DataField="SourceDataWhereExpression" HeaderText="BUG数据过滤表达式" />
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:FormView ID="FormView1" runat="server" DefaultMode="Insert" DataSourceID="DataSource" OnItemInserting="FormView1_ItemInserting" OnItemInserted="FormView1_ItemInserted">
        <InsertItemTemplate>
            名称:
            <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
            <br />
            代码:
            <asp:TextBox ID="CodeTextBox" runat="server" Text='<%# Bind("Code") %>' />
            <br />
            BUG变化检测表达式:
            <asp:TextBox ID="PredicateExpressionTextBox" runat="server" Text='<%# Bind("PredicateExpression") %>' />
            <br />
            BUG数据过滤表达式:
            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SourceDataWhereExpression") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
        </InsertItemTemplate>
    </asp:FormView>
</asp:Content>
