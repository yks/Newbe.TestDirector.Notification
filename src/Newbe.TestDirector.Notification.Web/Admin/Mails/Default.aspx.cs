﻿using Newbe.TestDirector.Notification.Web.Entities;
using System;

namespace Newbe.TestDirector.Notification.Web.Admin.Mails
{
    public partial class Default : System.Web.UI.Page
    {
        public IPrimaryKeyGenerator PrimaryKeyGenerator { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void FormView1_ItemInserting(object sender, System.Web.UI.WebControls.FormViewInsertEventArgs e)
        {
            e.Values.Add("Id", PrimaryKeyGenerator.GetKey());
        }

        protected void FormView1_ItemInserted(object sender, System.Web.UI.WebControls.FormViewInsertedEventArgs e)
        {
            GridView1.DataBind();
        }
    }
}
