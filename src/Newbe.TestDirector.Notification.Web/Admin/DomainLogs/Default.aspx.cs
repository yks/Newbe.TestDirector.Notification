﻿using Newbe.TestDirector.Notification.Web.Jobs;
using System;

namespace Newbe.TestDirector.Notification.Web.Admin.DomainLogs
{
    public partial class Default : System.Web.UI.Page
    {
        public ITdNotificationJob TdNotificationJob { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            TdNotificationJob.Notificat();
        }
    }
}
