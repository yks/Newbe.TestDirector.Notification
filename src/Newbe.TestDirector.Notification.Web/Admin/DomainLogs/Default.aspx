﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Newbe.TestDirector.Notification.Web.Admin.DomainLogs.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        div.logbody {
            max-height: 200px;
            overflow-y: scroll;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ef:EntityDataSource runat="server"
        ID="EfDataSource"
        ContextTypeName="Newbe.TestDirector.Notification.Web.Entities.DContext"
        EntitySetName="DomainEventLogs"
        EnableUpdate="True"
        EnableDelete="True"
        EnableInsert="True" OrderBy="it.Timestamp DESC">
    </ef:EntityDataSource>
    <asp:Button ID="btnSend" runat="server" Text="立即发件" OnClick="btnSend_Click" />
    <asp:GridView ID="GridView1" runat="server" DataSourceID="EfDataSource" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Timestamp" HeaderText="时间" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:TemplateField HeaderText="内容">
                <ItemTemplate>
                    <div class="logbody"><%# Eval("Message") %></div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
</asp:Content>
