﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using System.Collections.Generic;

namespace Newbe.TestDirector.Notification.Web.Jobs
{
    public class MailerSenderBookItem
    {
        public MailerInfoEntity MailerInfo { get; set; }

        public Dictionary<MailSceneEntity, BugChangeResultCollection> MatchedScenes { get; } =
            new Dictionary<MailSceneEntity, BugChangeResultCollection>();
    }
}
