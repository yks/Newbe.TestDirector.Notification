﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Newbe.TestDirector.Notification.Web.Jobs
{
    public class MailerSenderBookCollection : IEnumerable<MailerSenderBookItem>
    {
        private readonly IList<MailerSenderBookItem> _list;

        public MailerSenderBookCollection()
        {
            _list = new List<MailerSenderBookItem>();
        }

        public void Add(MailerInfoEntity mailerInfoEntity, MailSceneEntity mailSceneEntity,
            BugChangeResultCollection bugChangeResultCollection)
        {
            var fir = _list.FirstOrDefault(x => x.MailerInfo.MailAddress == mailerInfoEntity.MailAddress);
            if (fir == null)
            {
                fir = new MailerSenderBookItem
                {
                    MailerInfo = mailerInfoEntity,
                };
                _list.Add(fir);
            }
            fir.MatchedScenes.Add(mailSceneEntity, bugChangeResultCollection);
        }

        public IEnumerator<MailerSenderBookItem> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
