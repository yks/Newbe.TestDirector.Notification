﻿using System;

namespace Newbe.TestDirector.Notification.Web.Jobs
{
    public interface IMyJob
    {
        TimeSpan ActTime { get; }
        string CronExpression { get; }
    }
}
