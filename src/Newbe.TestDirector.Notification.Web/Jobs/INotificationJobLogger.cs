﻿using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Newbe.TestDirector.Notification.Web.Jobs
{
    public interface INotificationJobLogger
    {
        void Add(NotificationJobContext notificationJobContext);
    }

    internal class NotificationJobLogger : INotificationJobLogger
    {
        void INotificationJobLogger.Add(NotificationJobContext notificationJobContext)
        {
            var logDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "NotificationLogs");
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            File.AppendAllText(Path.Combine(logDir, $"{DateTime.Now:yyyyMMhhHHmmss}.json"),
                JsonConvert.SerializeObject(notificationJobContext));
        }
    }

    public class NotificationJobContext
    {
        public BugChangeResultCollection ComparedBugChangeResultCollection { get; set; }

        public IList<NotificationMatchedMailerSceneInfo> NotificationMatchedMailerSceneInfos { get; set; } =
            new List<NotificationMatchedMailerSceneInfo>();
    }

    public class NotificationMatchedMailerSceneInfo
    {
        public MailSceneEntity MailSceneEntity { get; set; }
        public BugChangeResultCollection MatchedSceneBugChangeResultCollection { get; set; }
    }
}
