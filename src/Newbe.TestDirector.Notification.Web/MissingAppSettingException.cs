﻿using System;

namespace Newbe.TestDirector.Notification.Web
{
    public class MissingAppSettingException : Exception
    {
        public string AppSettingName { get; set; }

        public MissingAppSettingException(string appSettingName) : this($"appsetting中缺少name为{appSettingName}的配置",
            appSettingName)
        {
            AppSettingName = appSettingName;
        }

        public MissingAppSettingException(string message, string appSettingName) : base(message)
        {
            AppSettingName = appSettingName;
        }

        public MissingAppSettingException(string message, Exception innerException, string appSettingName) : base(
            message, innerException)
        {
            AppSettingName = appSettingName;
        }
    }
}
