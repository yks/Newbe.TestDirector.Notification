﻿using Autofac;
using Newbe.TestDirector.Notification.Web.Domain;
using Newbe.TestDirector.Notification.Web.Domain.Impl;

namespace Newbe.TestDirector.Notification.Web
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterAssemblyTypes(typeof(AutofacModule).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterType<BugDao>().AsSelf();
            builder.Register(x =>
            {
                var historyTestDirectorDatabaseStore = x.Resolve<IHistoryTestDirectorDatabaseStore>();
                var geTestDirectorAcceessDbs = historyTestDirectorDatabaseStore.GetLastest();
                var re = new BugTableColumnsFactory(geTestDirectorAcceessDbs);
                return re;
            }).As<IBugTableColumnsFactory>();
        }
    }
}
